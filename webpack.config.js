'use strict'

var path = require('path');
var webpack = require('webpack');
module.exports = {

    entry: ['babel-polyfill', './app/js/app.js'],
    output: {

        filename: 'app/dist/bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                include: [
                    path.resolve(__dirname, "app")
                ],
                loader: 'babel-loader',
                query: {
                    presets: 'es2015'
                }
            }
        ]
    },
    plugins: [],
    cache: false,
    watch: true,
    devtool: 'inline-source-map',
};
