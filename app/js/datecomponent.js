let moment = require('moment');
let PlotHelper = require('./plothelper');

module.exports = class DateComponent {

    constructor(bars,myCanvas){
        this.bars = bars;
        this.myCanvas = myCanvas;
        this.ctx = myCanvas.context;
        this.plotHelper = new PlotHelper();
    }

    drawDateLine() {
        this.ctx.beginPath();
        let height = this.myCanvas.height - 20;
        this.ctx.moveTo(0, height);
        this.ctx.lineTo(this.myCanvas.width, height);
        this.ctx.stroke();

    }

    drawDate() {

        this.writeDateText();
        this.drawDateLine();
    }

    _isXCloseToCanvasWidth(x){
      return x>=this.myCanvas.width;
    }
    writeDateText() {
        let scale = this.plotHelper.calculateXAxis(this.myCanvas).scale;
        this.ctx.font = this.plotHelper.calculateDateFontSize() + ' roboto';
        this.ctx.fillStyle = 'black';
        for (let index = 0,_x=0; index <this.bars.length; index++) {

            if(index<=(this.bars.length-1)) {
                this.ctx.strokeText(moment(this.bars[index].date).format('DD/MM'),
                    _x, this.myCanvas.height-10);
                _x = _x + scale;
            }
            else  {
                //TODO:calculate em to px
                this.ctx.strokeText(moment(this.bars[this.bars.length].date).
                format('DD/MM'), (this.myCanvas.width - 25), this.myCanvas.height-10);
            }

        }
    }

};
