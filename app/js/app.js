let _ = require('lodash');
let ChartView = require('./chartview');


window.onload =function(){new App(window).main()};

class App {

    constructor(window) {
        this.window = window;
        this.index =0;
    }

    checkNotNull(obj,msg){

        if (_.isUndefined(obj) || _.isNull(obj)) {
            throw new Error(msg);
        }
        return this;
    }

    main() {
        console.log("Initialising chart");
        let canvasElement =document.getElementById("my-chart");
        this.checkNotNull(canvasElement,"Canvas element my-chart should be configured");
        let ctx = canvasElement.getContext('2d');
        this.checkNotNull(ctx,"Contex 2d should be present in the browser for drawing canvas");
        this.drawCanvas(canvasElement, ctx)
        var self = this;
        this.window.onresize = function () {
            self.drawCanvas(canvasElement, ctx);
        };
    }

    drawCanvas(canvasElement, ctx) {
        let view = new ChartView(canvasElement, ctx);
        view.update(0);
        let mouseDown = false,
            updateInProgress=false;
        canvasElement.addEventListener("mousedown", function (e) {
            if (e.target.tagName.match(/input|textarea|select/i)) {
                return;
            }
            mouseDown = true;
        }, false);

        document.addEventListener("mousemove", function (e) {
            if (!mouseDown) {
                return;
            }

            setTimeout(function(){
                if(!updateInProgress){
                    updateInProgress = true;
                    view.update(e.pageX);
                    updateInProgress =false;

                }
            },500);
            mouseDown = true;
        }, false);

        document.addEventListener("mouseup", function (e) {
            if (!mouseDown) {
                return;
            }
            mouseDown = false;
        }, false);
    }
}


