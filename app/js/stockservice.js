const QUERY = "http://query.yahooapis.com/v1/public/" +
    "yql?q=select%20*%20from%20csv%20where%20url%3D%27http%3A%2F%2Fichart.finance.yahoo.com%2Ftable.csv%3Fs%3DIBM%26d%3D5%26e%3D21%26f%3D201" +
    "6%26g%3Dd%26a%3D0%26b%3D2%26c%3D1962%26ignore%3D.csv%27&format=json";
let axios = require('axios');
let _ = require('lodash');
const MOCK = "mock/response.json"
module.exports = class StockService {
    constructor() {

    }

    getData() {
        return axios.get(MOCK).then(response => {
            return Promise.resolve(this._mapBarDataSet(response.data.query.results.row));

        }).catch(error => {
            return Promise.reject(error);
        });
    }

    _mapBarDataSet(rows) {

        let results = [];
        //"col0":"Date","
        // col1":"Open",
        // "col2":"High",
        // "col3":"Low"
        // ,"col4":
        // "Close",
        // "col5":"Volume",
        // "col6":"Adj Close"
        rows.map((row, index)=> {
            if (index > 0) {
                results.push({
                    date: row.col0,
                    open: row.col1,
                    high: row.col2,
                    low: row.col3,
                    close: row.col4,
                   volume: row.col5,
                   adj_close:row.col6
                });
            }
        });
        return results;
    }

}