let StockService = require('./stockservice');
let DateComponent = require('./datecomponent');
let PlotHelper = require('./plothelper');
module.exports = class CandleChart {

    constructor() {
        this.plotHelper = new PlotHelper();
        this.stockService = new StockService();
        this.barDataSet = null;

    }

    adjustIndex(index){
        if(index<0){
            return 0;
        }else if(index>=(this.barDataSet.length-30)){
            return (this.barDataSet.length-30);
        }else {
            return index;
        }
    }

    drawChart(_index,myCanvas) {
        if(this.barDataSet===null) {
            console.log(" I am null");
            let start = 0,end =this.plotHelper.calculateNoOfTicks();
            this.stockService.getData().then(barDataSet => {
                this.barDataSet = barDataSet;
                let bars = this.barDataSet.slice(start, this.plotHelper.calculateNoOfTicks());
                this._drawChart(bars,myCanvas);
                console.log(this.barDataSet.length);
            }).catch(error => {
                console.log(error);
                throw new Error(error);
            });
        }else {
            let index = this.adjustIndex(_index);
            console.log("calculated index %s",index);
            let start = index,end =index+ this.plotHelper.calculateNoOfTicks();
            let bars = this.barDataSet.slice(start,end);
            console.log(bars);
            this._drawChart(bars,myCanvas);
        }
    }

    _getBarWidth() {
        return 10;
    }
    ;

    _getBarHeight() {
        return 50;
    }
    ;

    _drawChart(bars,myCanvas) {
        let dateComponent = new DateComponent(bars,myCanvas);
        dateComponent.drawDate();
        let yConfig = this.plotHelper.calculateYAxis(bars,myCanvas);
        let xConfig = this.plotHelper.calculateXAxis(myCanvas);
        let _x = 0;
        for (let i = 0; i < bars.length; i++) {
            this._drawBar(
                {x: _x, yConfig: yConfig, data: bars[i]},myCanvas);
            _x = _x + xConfig.scale;
        }

    }

    _getColor(bar) {
        if ((bar.close - bar.open) > 0) {
            return 'green';
        } else {
            return 'red';
        }

    }

    _findYStartAndEnd(bar) {
        if (bar.open > bar.close) {
            return {
                start: bar.close,
                end: bar.open
            }
        } else {
            return {
                start: bar.open,
                end: bar.close
            }

        }
    }

    _calculateY(yConfig, bar,myCanvas) {
        let priceHeight =myCanvas.height - 100;
        let start = priceHeight - ((bar.open - yConfig.min) * yConfig.scale);
        let end = ((bar.open - bar.close) * yConfig.scale);
        return {
            start: start,
            end: end
        }
    }

    _drawBar(bar,myCanvas) {

        let ctx = myCanvas.context;
        let y = this._calculateY(bar.yConfig, bar.data,myCanvas);
        ctx.fillStyle = this._getColor(bar.data);
        ctx.fillRect(bar.x, y.start, this._getBarWidth(),
            y.end);
        let priceHeight =myCanvas.height - 100;
        ctx.fillStyle = "grey";
        let mid = this._getBarWidth() / 2;
        ctx.fillRect(bar.x + mid,
            priceHeight - (bar.yConfig.scale * (bar.data.low - bar.yConfig.min)),
            2,
            (bar.yConfig.scale * (bar.data.low -
            this._findYStartAndEnd(bar.data).start)));

        ctx.fillRect(bar.x + mid,
            priceHeight - (bar.yConfig.scale * (bar.data.high - bar.yConfig.min)),
            2,
            (bar.yConfig.scale * (bar.data.high-
            this._findYStartAndEnd(bar.data).end)));

    }

}
;