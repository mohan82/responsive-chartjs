let moment = require('moment');

var DateUtil = {};

DateUtil.getDates = function () {
    var dates = [], d = moment();
    _.range(10).forEach(function () {
        dates.push(d.format('DD/MM'));
        d.subtract(1, 'Days');
    });
    return dates;
};

module.exports = DateUtil;