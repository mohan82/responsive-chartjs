let MyCanvas = require('./mycanvas');
let CandleChart  = require('./candlechart');
let konva = require('konva');
let PlotHelper = require('./plothelper');

module.exports = class ChartView {
    constructor(canvasElement, ctx){
        this.canvasElement =  canvasElement;
        this.ctx = ctx;
        this.currentLeft=0;
        this.currentIndex =0;
        this.wrapperElement =  $("#canvas-wrapper");
        this.plotHelper = new PlotHelper();
        this.candleChart=  new CandleChart();
    }
    resizeCanvas() {
        this.canvasElement.width = this.wrapperElement.width();
        this.canvasElement.height = this.wrapperElement.height();

    };

    update (index) {
        console.log("Resizing..");
        this.currentIndex = index;
        this.resizeCanvas();
        this.clearCanvas();
        console.log("index %s",index);
        this.drawChart(index);
    };



    updateChartData(left){
        if(left>this.currentLeft) {
            console.log(`greater :${left}`);
            this.currentLeft =left;
            this.currentIndex =this.currentIndex+this.plotHelper.calculateNoOfTicks();
            this.update(this.currentIndex);

        }
        else if(left<this.currentLeft){
            console.log(`less :${left}`);
            this.currentLeft =left;
            this.currentIndex =this.currentIndex-this.plotHelper.calculateNoOfTicks();
            this.update(this.currentIndex);
        }
    }

    scroll(left){
     console.log("Scrolling...%s:",left);
        this.updateChartData(left);

    }

    clearCanvas () {
        this.ctx.clearRect(0, 0,  this.canvasElement.width,  this.canvasElement.height);
    }

    drawChart (index) {
        let myCanvas = new MyCanvas( this.canvasElement.width,  this.canvasElement.height,this.ctx);
        this.candleChart.drawChart(index,myCanvas);
    }
};
