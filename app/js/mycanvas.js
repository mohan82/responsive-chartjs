module.exports = class MyCanvas {
    constructor(width, height, context) {
        this.width = width;
        this.height = height;
        this.context = context;
    }
}