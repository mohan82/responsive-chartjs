let _ = require('lodash');

let DEVICEINFO = {
    XS: {
        width: 480,
        noOfTicks: 10,
        dateFontSize: '0.7em',
    },
    SM: {
        width: 768,
        dateFontSize: '0.7em',
        noOfTicks: 10
    },
    MD: {
        width: 992,
        noOfTicks: 15,
        dateFontSize: '1em',

    },
    LG: {
        width: 1200,
        noOfTicks: 15,
        dateFontSize: '1em',
    },
    getDeviceInfo(windowWidth){
        if (windowWidth >= this.MD.width) {
            return this.LG;
        } else if (windowWidth < this.MD.width &&
            windowWidth >= this.SM.width) {
            return this.MD;
        } else if (windowWidth < this.SM.width && windowWidth >= this.XS.width) {
            return this.SM;
        } else {
            return this.XS;
        }
    }
};

module.exports = class PlotHelper {


    _getDeviceInfo() {
        return DEVICEINFO.getDeviceInfo(window.innerWidth);
    }

    calculateNoOfTicks() {
        return this._getDeviceInfo()
            .noOfTicks;

    }

    calculateXAxis(myCanvas){
        let scale = myCanvas.width /this.calculateNoOfTicks();
        return {
            scale:scale
        };
    }
    calculateYAxis(dataSet,myCanvas){
        let max = _.maxBy(dataSet,'high').high;
        let min = _.minBy(dataSet,'low').low;
        let range = max -min;
        return {
            max:max,
            min:min,
            range:range,
            scale:((myCanvas.height-70)/range)

        };
    }

    calculateDateFontSize() {
        return this._getDeviceInfo().dateFontSize;
    }


    calculateWidth() {

    }

    calculateHeight() {
    }
};

